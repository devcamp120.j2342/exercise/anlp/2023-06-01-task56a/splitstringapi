package com.devcamp.splitstringapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class SplitStringAPI {
    @GetMapping("/split")
    public ArrayList<String> splitString(@RequestParam(required = true, name = "String") String requeString){
        ArrayList<String> splitStringArray = new ArrayList<>();
        String[] stringArray = requeString.split(" ");

        for (String stringElemeString : stringArray) {
            splitStringArray.add(stringElemeString);
        }

        return splitStringArray;
    }
}
